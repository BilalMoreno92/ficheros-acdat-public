package com.example.ficheros;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EscribirExternaActivity extends AppCompatActivity {

    private static final int REQUEST_WRITE = 1;
    public static final String NOMBRE_FICHERO = "resultado.txt";
    public static final String CODIFICACION = "UTF-8";
    Memoria memoria;

    @BindView(R.id.edTexto)
    EditText edTexto;

    @BindView(R.id.tvPropiedades)
    TextView tvPropiedades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escribir_externa);

        memoria = new Memoria(getApplicationContext());

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btGuardar)
    public void guardar() {
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
        // pedir los permisos necesarios, porque no están concedidos
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE);
        // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
        } else {
        // Permisos ya concedidos
            escribir();
        }

    }

    private void escribir() {
        if (memoria.disponibleEscritura()){
            if (memoria.escribirExterna(NOMBRE_FICHERO, edTexto.getText().toString(), false, CODIFICACION)){
                tvPropiedades.setText(memoria.mostrarPropiedadesExterna(NOMBRE_FICHERO));
            } else {
                tvPropiedades.setText("Error al escribir en el fichero " + NOMBRE_FICHERO);
            }
        } else {
            tvPropiedades.setText("Memoria externa no disponible");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
// chequeo los permisos de nuevo
        switch (requestCode) {
            case REQUEST_WRITE:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // permiso concedido
                    escribir();
                } else {
                    // no hay permiso
                    Toast.makeText(this, "No hay permiso para escribir en la memoria externa", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
