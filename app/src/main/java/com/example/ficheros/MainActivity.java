package com.example.ficheros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.btInterna, R.id.btExterna, R.id.btMaterialFilePicker, R.id.btLectura, R.id.btExplorar})
    public void openActivity(Button button){
        Intent intent = null;
        switch (button.getId()){
            case R.id.btInterna:
                intent = new Intent(this, EscribirInternaActivity.class);
                break;
            case R.id.btExterna:
                intent = new Intent(this, EscribirExternaActivity.class);
                break;
            case R.id.btMaterialFilePicker:
                intent = new Intent(this, MaterialFilePickerActivity.class);
                break;
            case R.id.btLectura:
                intent = new Intent(this, LecturaActivity.class);
                break;
            case R.id.btExplorar:
                intent = new Intent(this, ExplorarActivity.class);
                break;
        }
        startActivity(intent);
    }
}
