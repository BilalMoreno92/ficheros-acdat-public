package com.example.ficheros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ExplorarActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int OPEN_FILE_REQUEST_CODE = 1;
    private static final String CODIFICACION = "UTF-8";

    private Button btAbrir;
    private TextView tvRuta;
    private TextView tvContenido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorar);

        btAbrir = findViewById(R.id.btExplorar);
        btAbrir.setOnClickListener(this);
        tvRuta = findViewById(R.id.tvContenido);
        tvContenido = findViewById(R.id.tvFichero);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, OPEN_FILE_REQUEST_CODE);
        else
            //informar que no hay ninguna aplicación para manejar ficheros
            Toast.makeText(this, "No hay aplicación para manejar ficheros", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String path, mensaje = "";
        Resultado resultado;
        Memoria memoria = new Memoria(this);

        if (requestCode == OPEN_FILE_REQUEST_CODE)
            if (resultCode == RESULT_OK) {
                // Mostramos en la etiqueta la ruta del archivo seleccionado
                path = data.getData().getPath();
                tvRuta.setText(path);
                if (path != null) {
                    //leer el fichero
                    resultado = memoria.leerRutaExterna(path, CODIFICACION);
                    if (resultado.getCodigo()) {
                        tvContenido.setText(resultado.getContenido());
                        mensaje = "Fichero leido correctamente";
                    } else {
                        tvContenido.setText("");
                        mensaje = "Error al leer: " + resultado.getMensaje();
                    }
                }
            } else {
                mensaje = "Error: " + resultCode;
                tvRuta.setText("");
            }
        if (!mensaje.equals("")) {
            Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
        }
    }
}
